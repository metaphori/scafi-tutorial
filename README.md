# README #

This repo is intended to provide supporting material for tutorials on *scafi*, a *Scala framework for Aggregate Programming*.


### Package `splash16` ###

The code is arranged to make you able to play with Aggregate Programming without pain.

* `AggregatePrograms` module contains a set of predefined aggregate programs.
* `Networks` module contains predefined networks.
* `Runner` is the main program. Open it, select your program/network for the simulation, and run it. 